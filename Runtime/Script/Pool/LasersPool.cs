﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LasersPool : MonoBehaviour
{
    public Vector3 m_offsetBlender;
    private static LasersPool m_instance;
    public static LasersPool GetInstanceInScene() { return m_instance; }
    internal static LasersInScene ClaimAvailaibleLaser()
    {
        CheckAndCreateInstanceIfNull();
        LasersInScene[] lasers = GetLasers();
        for (int i = 0; i < lasers.Length; i++)
        {
            if (!lasers[i].IsClaim()) {
                lasers[i].Claim();
                return lasers[i];
            }
        }
        return null;
    }

    public Vector3 m_rotationOffset;

    public static void CheckAndCreateInstanceIfNull() {
        if (m_instance == null)
            CreateLaserPoolInstance();
    }

    public static GameObject CreateLaserPoolInstance() {
       GameObject poolPrefab = Resources.Load<GameObject>("LasersPool/LasersPool");
        return GameObject.Instantiate(poolPrefab);
    }

    public static int GetLasersCount() {

        CheckAndCreateInstanceIfNull();
        if (m_instance == null) return 0;
        return m_instance.m_lasersInScene.Count; }
    public static LasersInScene GetLaser(int index)
    {
        CheckAndCreateInstanceIfNull();
        index = Mathf.Clamp(index, 0, GetLasers().Length)-1;
        return m_instance.m_lasersInScene[index];
    }
    public static LasersInScene [] GetLasers() {
        CheckAndCreateInstanceIfNull();
        return GetInstanceInScene().m_lasersInScene.ToArray(); }

    [SerializeField]
    private List<LasersInScene> m_lasersInScene = new List<LasersInScene>();



    public void Awake()
    {
        m_instance = this;
        UnclaimAll();
    }

    private void UnclaimAll()
    {
        for (int i = 0; i < m_lasersInScene.Count; i++)
        {
            m_lasersInScene[i].Unclaim();

        }
    }

    [Header("Laser Quick Add")]
    public List<Transform> m_endPointLaser;
    public List<Transform> m_startPointLaser;

    
    public void OnValidate()
    {
        int maxIndex = m_startPointLaser.Count;
        if (m_endPointLaser.Count < m_startPointLaser.Count)
            maxIndex = m_endPointLaser.Count;

        for (int i = maxIndex-1; i >=0 ; i--)
        {
            m_lasersInScene.Add(new LasersInScene(m_startPointLaser[i], m_endPointLaser[i]));
        }
        if (maxIndex > 0) {
            m_startPointLaser.RemoveRange(0, maxIndex);
            m_endPointLaser.RemoveRange(0, maxIndex);

        }
    }
}

[System.Serializable]
public class LasersInScene : IClaimable, IPoolable
{
    public bool m_isClaimed;
    public Transform m_startPointLaser;
    public Transform m_endPointLaser;
    public Quaternion m_startInitialRotation ;
    public Quaternion m_endInitialRotation;

    public LasersInScene(Transform startPointLaser, Transform endPointLaser)
    {
        this.m_startPointLaser = startPointLaser;
        this.m_endPointLaser = endPointLaser;
    }

    public void SetEndPointAt(Vector3 position) {
        SetPositions(m_startPointLaser.position, position);
    }
    public void SetStartPointAt(Vector3 position, Quaternion direction)
    {
        SetPositions(position ,m_endPointLaser.position);
    }
    public void SetPositions(Vector3 startPosition, Vector3 endPosition)
    {
        m_startPointLaser.position = startPosition;
        m_endPointLaser.position = endPosition;
        m_startPointLaser.LookAt(m_endPointLaser, Vector3.up);
        m_endPointLaser.LookAt(m_startPointLaser, Vector3.down);
        m_startPointLaser.rotation *= m_startInitialRotation;//Quaternion.Euler(LasersPool.GetInstanceInScene().m_offsetBlender);
        m_endPointLaser.rotation *= Quaternion.Inverse( m_endInitialRotation);//Quaternion.Inverse( Quaternion.Euler(LasersPool.GetInstanceInScene().m_offsetBlender));
    }

    internal void Translate(Vector3 direction, Space space)
    {
        m_startPointLaser.Translate(direction, space);
        m_endPointLaser.Translate(direction, space);
    }
    public void Claim() {
        ResetToDefaultValue();
        m_startInitialRotation = m_startPointLaser.localRotation;
        m_endInitialRotation = m_endPointLaser.localRotation; 
        m_isClaimed = true;
    }

    public bool IsClaim()
    {
        return m_isClaimed;
    }

    public void Unclaim()
    {
        Hide();
        ResetToDefaultValue();
        m_isClaimed = false;
        
    }
    public void ResetToDefaultValue()
    {
        if (m_startPointLaser && m_endPointLaser) {
            m_startPointLaser.localScale = Vector3.one;
            m_endPointLaser.localScale = Vector3.one;

        }
    }

    internal void SetSize(float laserSize)
    {
        m_startPointLaser.localScale = Vector3.one * laserSize;
        m_endPointLaser.localScale = Vector3.one * laserSize;
    }

    public void Hide()
    {
        if (m_startPointLaser == null || m_endPointLaser == null)
            return;
        m_startPointLaser.position = Vector3.one * 20000f;
        m_endPointLaser.position = Vector3.one * 20000f;
        m_startPointLaser.localScale =Vector3.zero;
        m_endPointLaser.localScale = Vector3.zero;
    }
}

public interface IPoolable {

    void ResetToDefaultValue();
    void Hide();

}

public interface IClaimable
{
    void Claim();
    bool IsClaim();
    void Unclaim();
}