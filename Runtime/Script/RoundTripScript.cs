﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundTripScript : MonoBehaviour
{
    public float m_speed=1;
    public Transform m_direction;
    public Transform m_affected;
    public LayerMask m_collisionLayer;
    public float m_radius = 0.1f;

    void Update()
    {
        this.m_affected.position += m_direction.forward * Time.deltaTime * m_speed ;
        
    }
    private IEnumerator Start()
    {
        while (true) {
            yield return new WaitForSeconds(0.1f);
            if (CheckForCollision()) {
                Reverse();

                yield return new WaitForSeconds(1f);
            }
        }
    }
    
    private bool CheckForCollision()
    {

           return  Physics.OverlapSphere(m_affected.position, m_radius, m_collisionLayer).Length>0;


    }


    public void Reverse() {
        m_direction.forward = -m_direction.forward;
    }
}
