﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScript : MonoBehaviour
{
    public float m_speed= 90;
    public Axis m_axis;
    public enum Axis { X, Y, Z }

    public void Update()
    {

        if (m_axis == Axis.X)
            transform.Rotate(transform.right * m_speed * Time.deltaTime);
        if (m_axis == Axis.Y)
            transform.Rotate(transform.up * m_speed * Time.deltaTime);
        if (m_axis == Axis.Z)
            transform.Rotate(transform.forward * m_speed * Time.deltaTime);
    }
}
