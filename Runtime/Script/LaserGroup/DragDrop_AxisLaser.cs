﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDrop_AxisLaser : BasicLaser
{
    public Transform m_direction;
    public LayerMask m_allowToFixTo;
    public Axis m_axis;
    public enum Axis { X, Y, Z}
    public Space m_spaceType;
    private new void LateUpdate()
    {
        base.LateUpdate();
        SetNewPosition(transform.position);
    }
   
    public void SetNewPosition(Vector3 position)
    {

        Vector3 dir= Vector3.one;
        if (m_spaceType == Space.World)
        {
            if (m_axis == Axis.X)
                dir = Vector3.left;
            if (m_axis == Axis.Y)
                dir = Vector3.up;
            if (m_axis == Axis.Z)
                dir = Vector3.forward;
        }
        else {
            if (m_axis == Axis.X)
                dir = m_direction.right;
            if (m_axis == Axis.Y)
                dir = m_direction.up;
            if (m_axis == Axis.Z)
                dir = m_direction.forward;
        }
      //  DebugDraw();


        RaycastHit hit;
     //   Debug.DrawRay(position, dir, Color.red, 60);
        if (Physics.Raycast(position, dir, out hit, float.MaxValue, m_allowToFixTo))
        {
            m_start.position = hit.point;
        }
        else m_start.position = m_direction.position + dir * 20000f;

        if (Physics.Raycast(position, -dir, out hit, float.MaxValue, m_allowToFixTo))
        {
            m_end.position = hit.point;
        }
        else m_end.position = m_direction.position + -dir * 20000f;


    }
}
