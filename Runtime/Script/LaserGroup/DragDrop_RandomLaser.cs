﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDrop_RandomLaser : BasicLaser
{
    public LayerMask m_allowToFixTo;
    private new void Start()
    {
        base.Start();
        SetNewRandomPosition(transform.position);
    }

    public void SetNewRandomPosition(Vector3 position)
    {
        Quaternion rotation = GetRandomQuad();
        RaycastHit hit;
        transform.rotation = rotation;
        // Debug.DrawRay(position, rotation * Vector3.forward, Color.red, 60);
        if (Physics.Raycast(position, transform.forward, out hit, float.MaxValue, m_allowToFixTo))
        {
            m_start.position = hit.point;
        }
        else
            m_start.position = position + 1000 * transform.forward;
        if (Physics.Raycast(position, -transform.forward, out hit, float.MaxValue, m_allowToFixTo))
        {
            m_end.position = hit.point;
        }
        else
            m_end.position = position + -1000 * transform.forward;


    }

    private static Quaternion GetRandomQuad()
    {
        return new Quaternion(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
    }

    private new void LateUpdate()
    {
        base.LateUpdate();

    }
}
