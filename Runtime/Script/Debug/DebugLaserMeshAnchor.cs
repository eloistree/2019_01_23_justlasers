﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugLaserMeshAnchor : MonoBehaviour
{
    public static List<DebugLaserMeshAnchor> m_registeredInScene = new List<DebugLaserMeshAnchor>();
    private static bool m_allSwitchState = false;
    public GameObject[] m_toSwitch;
    public bool m_startState;
    public void Awake()
    {
        SetDebugAs(m_startState);
        m_registeredInScene.Add(this);
    }
    public void OnDestroy()
    {
        m_registeredInScene.Remove(this);
    }

    public static void SetAllDebugAs(bool value) {
        m_allSwitchState = value;
        for (int i = 0; i < m_registeredInScene.Count; i++)
        {
            m_registeredInScene[i].SetDebugAs(value);
        }
    }


    public static void SwitchAll()
    {
        DebugLaserMeshAnchor.SetAllDebugAs(!DebugLaserMeshAnchor.GetAllDebugState());
        
    }
  
    public static bool GetAllDebugState()
    {
        return m_allSwitchState;
    }

   
    public  void SetDebugAs(bool value) {
        for (int i = 0; i < m_toSwitch.Length; i++)
        {
            m_toSwitch[i].SetActive(value);
        }
    }

}
