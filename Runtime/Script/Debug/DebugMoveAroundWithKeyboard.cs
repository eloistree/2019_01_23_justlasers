﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMoveAroundWithKeyboard : MonoBehaviour
{

    public Transform m_affected;
    public Transform m_rotationCenter;
    public float m_verticalSpeed=0.5f;
    public float m_horizontalSpeed=1;
    public float m_forwardSpeed=2;
    public float m_backwardSpeed=1;
    public float m_rotationAngle = 20f;

    void Update()
    {
        
        UpdateWindow();


    }

    private void UpdateWindow()
    {
        float rotation = 0;
        Vector3 direction = Vector3.zero;
        direction.x += Input.GetAxis("Horizontal") * m_horizontalSpeed;
        if (Input.GetKey(KeyCode.Keypad4) && !Input.GetKey(KeyCode.Keypad6))
        {
            direction.x = -1f * m_horizontalSpeed;
        }
        if (Input.GetKey(KeyCode.Keypad6) && !Input.GetKey(KeyCode.Keypad4))
        {
            direction.x = 1f * m_horizontalSpeed;
        }

        

        direction.y += Input.GetAxis("Vertical") * m_verticalSpeed;
        if (Input.GetKey(KeyCode.Keypad8))
        {
            direction.y += 1f * m_horizontalSpeed;
        }
        if (Input.GetKey(KeyCode.Keypad5))
        {
            direction.y += -1f * m_horizontalSpeed;
        }

        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Keypad0))
            direction.z += m_forwardSpeed;
        if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Keypad1))
            direction.z -= m_backwardSpeed;

        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad7) || Input.GetMouseButtonDown(0))
            rotation += m_rotationAngle;
        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad9) || Input.GetMouseButtonDown(1))
            rotation -= m_rotationAngle;

        direction *= Time.deltaTime;

        m_affected.RotateAround(m_rotationCenter.position, Vector3.up, rotation);
        m_affected.Translate(direction);
    }
    
}
