﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LasersMoveTextureMaterial : MonoBehaviour
{
    public Material m_moveVertex;
    public Vector2 m_movePosition;
    public Vector2 m_moveSpeed;

    void Update()
    {

        m_movePosition += m_moveSpeed*Time.deltaTime;
        m_moveVertex.SetTextureOffset("_MainTex", m_movePosition);
    }
}
