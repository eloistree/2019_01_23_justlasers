﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class LasersListener : MonoBehaviour
{
    public LayerMask m_lookingFor = ~0;
    public bool m_onlyLaserSensitive;
    public float m_interval=0.05f;
    public ILaser[] lasersInScene;
    public bool m_isHittingObject;

    [SerializeField]  UnityEvent m_onAnyLaserHit;
    [Tooltip("Using Physic.Raycast()")]
    [SerializeField]  LaserHitEvent m_onLaserDirectHit;
    [Tooltip("Using Physic.RaycastAll()")]
    [SerializeField]  LaserHitEvent m_onLaserPassthroughHit;

    IEnumerator Start()
    {
        while (true) {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_interval);
            lasersInScene = FindObjectsOfType<MonoBehaviour>().OfType<ILaser>().ToArray();
                m_isHittingObject = false;
            foreach (ILaser laser in lasersInScene)
            {
                RaycastHit hit;
                bool isHitting = laser.CheckCollision(out hit, m_lookingFor);
                if (isHitting) {
                   
                 
                    LaserHit lh = new LaserHit();
                    lh.m_collider = hit.collider;
                    lh.m_point = hit.point;
                    lh.m_laser = laser;
                    lh.m_usedMask = m_lookingFor;
                    if (!m_onlyLaserSensitive || (m_onlyLaserSensitive && lh.IsLaserSensitive())) {

                        Debug.DrawLine(hit.collider.transform.position, hit.point, Color.magenta, 500);
                        m_isHittingObject = true;
                        m_onLaserDirectHit.Invoke(lh);

                        break;
                    }
            
                    }

                RaycastHit[] rayHits;
                if (laser.CheckCollisions(out rayHits, m_lookingFor)) {
                    for (int i = 0; i < rayHits.Length; i++)
                    {
                        LaserHit lh = new LaserHit();
                        lh.m_collider = rayHits[i].collider;
                        lh.m_point = rayHits[i].point;
                        lh.m_laser = laser;
                        lh.m_usedMask= m_lookingFor;
                        if (!m_onlyLaserSensitive || (m_onlyLaserSensitive &&  lh.IsLaserSensitive()) )
                            m_onLaserPassthroughHit.Invoke(lh);

                    }

                }
            }


            if (m_isHittingObject)
                m_onAnyLaserHit.Invoke();
        }
        
    }
    
}

[System.Serializable]
public class LaserHitEvent : UnityEvent<LaserHit> { }

[System.Serializable]
public class LaserHit {
    public ILaser m_laser;
    public LayerMask m_usedMask;
    public Collider m_collider;
    public Vector3 m_point;
    public bool IsLaserSensitive(out ILaserSensitive obj) {
        obj = m_collider.GetComponent<ILaserSensitive>();
        return obj != null;
    }

    internal bool IsLaserSensitive()
    {
        ILaserSensitive ls;
        return IsLaserSensitive(out ls);
    }
}

