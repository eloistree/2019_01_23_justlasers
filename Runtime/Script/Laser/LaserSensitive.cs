﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILaserSensitive
{
    GameObject GetLinkedGameObject();
    Transform GetLinkedTransform();
}

public class LaserSensitive : MonoBehaviour, ILaserSensitive
{
    public GameObject GetLinkedGameObject()
    {
        return gameObject;
    }

    public Transform GetLinkedTransform()
    {
        return transform;
    }
    
}
