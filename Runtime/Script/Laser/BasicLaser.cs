﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface ILaser {

     Vector3 GetDirectionFromStart(bool normilized = false);
     Vector3 GetStartPosition();
     Vector3 GetEndPosition();
     Vector3 GetDirectionFromEnd(bool normilized = false);
     float GetDistance();

     bool CheckCollision(out RaycastHit hit, LayerMask mask);
     RaycastHit[] CheckAroundLaser(float radius, LayerMask mask);
    bool CheckCollisions(out RaycastHit[] hits, LayerMask mask);

    void DebugDraw(float duration = 1f);
     void DebugDraw(Color color, float duration = 1f);
}

public class BasicLaser : MonoBehaviour , ILaser
{
    public LasersInScene m_laser;
    public Transform m_start;
    public Transform m_end;
    public float m_laserSize=1;
    protected void Start()
    {
        m_laser = LasersPool.ClaimAvailaibleLaser();
        m_laser.SetSize(m_laserSize);
        
    }
    protected void OnDestroy()
    {
        m_laser.Unclaim();
    }

    // Update is called once per frame
    protected void LateUpdate()
    {
        m_laser.SetPositions(m_start.position, m_end.position);

    }


    public Vector3 GetDirectionFromStart(bool normilized = false)
    {
        return normilized ? (m_end.position - m_start.position).normalized : (m_end.position - m_start.position);
    }

    public Vector3 GetStartPosition()
    {
        return m_start.position;
    }
    public Vector3 GetEndPosition()
    {
        return m_end.position;
    }

    public Vector3 GetDirectionFromEnd(bool normilized = false)
    {
        return -GetDirectionFromStart();
    }

    public float GetDistance()
    {
        return Vector3.Distance(m_start.position, m_end.position);
    }

    public void DebugDraw( float duration = 1f)
    {
        DebugDraw(Color.red, duration);

    }
    public void DebugDraw(Color color, float duration = 1f)
    {
        Debug.DrawLine(m_start.position, m_end.position, color, duration);
    }

    public bool CheckCollision()
    {
        RaycastHit hit;
        return CheckCollision(out hit);
    }
    public bool CheckCollision(out RaycastHit hit)
    {
        return CheckCollision(out hit, ~0);
    }

    public bool CheckCollision(out RaycastHit hit, LayerMask mask)
    {
        return Physics.Raycast(GetStartPosition(), GetDirectionFromStart(), out hit, GetDistance(), mask);
    }
    public bool CheckCollisions(out RaycastHit [] hits, LayerMask mask)
    {
        hits = Physics.RaycastAll(GetStartPosition(), GetDirectionFromStart(), GetDistance(), mask);
        return hits.Length > 0;
    }


    public RaycastHit[] CheckAroundLaser(float radius, LayerMask mask)
    {
        Vector3 dir = GetDirectionFromStart(false);
        Vector3 s = GetStartPosition() + dir.normalized * (0.1f + radius);
        Vector3 e = GetEndPosition() - (dir.normalized * (0.1f + radius));
        Debug.DrawLine(s, e, Color.cyan, 5);
        return Physics.SphereCastAll(s, radius, dir.normalized, Vector3.Distance(s, e), mask);

    }


}
