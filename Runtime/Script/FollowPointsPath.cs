﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPointsPath : MonoBehaviour
{
    public Transform m_affected;
    public Transform[] m_pointsToFollow;
    public float m_speed=30;
    public enum LoopType { Loop, Boomerang}
    public LoopType m_loopType;



    public int m_index;
    public bool m_foward=true;

    public Vector3 m_startPosition;
    public Vector3 m_endPosition;
    public Quaternion m_startRotation;
    public Quaternion m_endRotation;
    public float m_timeToNextPoint;
    public float m_currentTime;

    private bool isDefined;
    void SetNewMove(Transform from,Transform to)
    {
        m_startPosition = from.position;
        m_startRotation = from.rotation;
        m_endPosition = to.position;
        m_endRotation = to.rotation;
        float distance = Vector3.Distance(from.position, to.position);
        m_currentTime = m_timeToNextPoint = distance / m_speed;
        isDefined = true;

    }

 

    // Update is called once per frame
    void Update()
    {
        int pointsCount = m_pointsToFollow.Length;
        if (pointsCount<= 1) return;
        m_currentTime -= Time.deltaTime;
        if (isDefined) {
            m_affected.position = Vector3.Lerp(m_startPosition, m_endPosition,1f-( m_currentTime / m_timeToNextPoint));
            m_affected.rotation = Quaternion.Lerp(m_startRotation, m_endRotation,1f-( m_currentTime / m_timeToNextPoint));
        }


        if (m_currentTime < 0f)
        {
            if (m_loopType == LoopType.Boomerang) {

                if (m_foward)
                {
                    m_index++;
                    if (m_index >= pointsCount-1)
                    {
                        m_index = pointsCount - 1;
                        m_foward = false;
                    }

                }else {
                    m_index--;
                    if (m_index <1) {
                        m_index = 0;
                        m_foward = true;
                    }

                }
                SetNewMove(m_pointsToFollow[m_index], m_pointsToFollow[m_index + (m_foward ? 1 : -1)]);

            }
            else if (m_loopType == LoopType.Loop)
            {
                int from = m_index;
                if (m_foward) ++m_index; else --m_index;
                
                if (m_index < 0)
                    m_index = pointsCount-1;
                if (m_index >= pointsCount)
                    m_index = 0;
                int to = m_index;
                SetNewMove(m_pointsToFollow[from], m_pointsToFollow[to]);

            }


        }
    }
}
