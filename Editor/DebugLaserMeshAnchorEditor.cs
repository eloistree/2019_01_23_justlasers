﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DebugLaserMeshAnchor))]
public class DebugLaserMeshAnchorEditor : Editor
{

        public override void OnInspectorGUI()
        {
            this.DrawDefaultInspector();
            DebugLaserMeshAnchor root = (DebugLaserMeshAnchor)target;
            GUILayout.BeginHorizontal();
            
            if (GUILayout.Button("On/Off All"))
                DebugLaserMeshAnchor.SwitchAll();
            GUILayout.EndHorizontal();

        }

    
}